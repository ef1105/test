using UnityEngine;
using System.Collections;

public class Player_Hit : MonoBehaviour {
	private int hp = 3;					//Health
	private bool dead;					//Are we dead
	private bool canBeHit = true;		//Can we be hit
	public GameObject mesh;				//Mesh
	public GameObject explosion;		//Explosion prefab
	
	IEnumerator Hit()
	{
		//We cant be hit
		canBeHit = false;
		
		//Dont show player
		mesh.renderer.enabled = false;
		//Wait 0.2 second
		yield return new WaitForSeconds(0.2f);
		
		//Show player
		mesh.renderer.enabled = true;
		//Wait 0.2 second
		yield return new WaitForSeconds(0.2f);
		
		//Dont show player
		mesh.renderer.enabled = false;
		//Wait 0.2 second
		yield return new WaitForSeconds(0.2f);
		
		//Show player
		mesh.renderer.enabled = true;
		//Wait 0.2 second
		yield return new WaitForSeconds(0.2f);
		
		//Dont show player
		mesh.renderer.enabled = false;
		//Wait 0.2 second
		yield return new WaitForSeconds(0.2f);
		
		//Show player
		mesh.renderer.enabled = true;
		
		//We can be hit
		canBeHit = true;
	}
	
	void OnTriggerEnter(Collider other)
	{
		//If we are in a enemy trigger
		if (other.tag == "Enemy")
		{
			//If we can be hit
			if (canBeHit)
			{
				//Remove 1 from hp
				hp--;
				
				//If hp is less than 1
				if (hp < 1)
				{
					//We are dead
					dead = true;
					
					//Dont show player
					//mesh.renderer.enabled = false;
					
					//Set collider to false
					collider.enabled = false;
				}
				//If hp is bigger than 0
				else
				{
					//Start Hit
					StartCoroutine("Hit");
				}
				
				//Instantiate explosion
				Instantiate(explosion,transform.position,Quaternion.identity);
				
				//Destroy
				Destroy(other.gameObject);
			}
		}
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
