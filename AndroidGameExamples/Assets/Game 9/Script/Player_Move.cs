using UnityEngine;
using System.Collections;

public class Player_Move : MonoBehaviour {
	private Vector3 pos;				//Position
	private float tmpFireTime;			//Tmp fire time
	public Transform[] bulletSpawnPos;	//Bullet spawn position
	public GameObject bullet;			//Bullet prefab
	
	// Use this for initialization
	void Start () {
	
	}
	
	void ShotUpdate()
	{
		//If tmpFireTime is bigger than 0.2
		if (tmpFireTime > 0.2f)
		{
			//If the game is running on a android device
			if (Application.platform == RuntimePlatform.Android)
			{
				//If finger is on screen
				if (Input.GetTouch(0).tapCount != 0)
				{
					//Go through all the bullet spawn position
					for (int i = 0; i < bulletSpawnPos.Length; i++)
					{
						//Instantiate bullet
						Instantiate(bullet, bulletSpawnPos[i].position, bulletSpawnPos[i].rotation);
						//Set tmpFireTime to 0
						tmpFireTime = 0;
					}
				}
				else
				{
					//Set tmpFireTime to 0.2
					tmpFireTime = 0.2f;
				}
			}
			//If the game is not running on a android device
			else
			{
				//If get left mouse button down
				if (Input.GetMouseButton(0))
				{
					//Go through all the bullet spawn position
					for (int i = 0; i < bulletSpawnPos.Length; i++)
					{
						//Instantiate bullet
						Instantiate(bullet,bulletSpawnPos[i].position, bulletSpawnPos[i].rotation);
						//Set tmpFireTime to 0
						tmpFireTime = 0;
					}
				}
				else
				{
					//Set tmpFireTime to 0.2
					tmpFireTime = 0.2f;
				}
			}
		}
		//If tmpFireTime is less than 0.2
		else
		{
			//Add 1 to tmpFireTime
			tmpFireTime += 1 * Time.deltaTime;
		}
	}
	
	void MoveUpdate()
	{
		//If the game is running on a android device
		if (Application.platform == RuntimePlatform.Android)
		{
			//Get screen position
			pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 1));
		}
		//If the game is not running on a android device
		else
		{
			//Get screen position
			pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
		}

		transform.position = new Vector3(pos.x,pos.y + 1f,pos.z);
	}
	
	// Update is called once per frame
	void Update () {
		MoveUpdate();
		ShotUpdate();
	}
}
